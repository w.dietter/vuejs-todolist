const vm = new Vue({
  el: "#todoList",
  data: {
    todoInput: "",
    todos: [],
    toggleSlider: true,
    index: 0
  },
  methods: {
    addTodo: function() {
      if (this.todoInput !== "") {
        this.todos.push({
          id: Math.random(),
          title: this.todoInput,
          isCompleted: false
        });
      }
      this.todoInput = "";
    },
    completeTodo: function(index) {
      console.log(this.todos[index].isCompleted);
      this.todos[index].isCompleted = !this.todos[index].isCompleted;
    },
    deleteTodo: function(id) {
      this.todos = this.todos.filter(todo => todo.id !== id);
    },
    incrementIndex() {
      if(this.index < this.todos.length - 1) {
        this.index = this.index + 1;
      }
    },
    decrementIndex() {
      if(this.index >= 1) {
        this.index = this.index - 1;
      }
    },
  },
  computed: {
    getTodo() {
      if(this.todos.length > 0) {
        return this.todos[this.index].title;
      }
      return null;
    }
  },
  getDisplayIndex() {
    if(this.todos.length === 0) {
      return 0;
    } else {
      return this.index;
    }
  }
});
